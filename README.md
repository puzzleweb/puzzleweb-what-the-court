# PuzzleWeb: What the Court

Ihr Tool für den Fall eines Wahlgerichtsstandes.

## Dateien

_cases_excerpt100.csv_ - Auszug aus dem gelabelten Datensatz

**Backend:**
- _court_cases_O-Ca-C.csv_ - Mittlere Kostenaufteilungen und Verfahrenszahlen nach Gerichten
- _court_ttest_pvals.csv_ - p-Werte aus den t-Tests der Gerichtspaare; Signifikanz der Unterschiede.

Ein Label von 0.0 bedeutet, dass der Kläger 100% der Kosten übernimmt.
Ein Label von 0.8 bedeutet, dass der Kläger 20% und der Beklagte 80% der Kosten übernimmt, etc.
